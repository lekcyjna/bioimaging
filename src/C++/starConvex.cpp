#include <stdexcept>
#include <xtensor/xshape.hpp>
#define XTENSOR_USE_XSIMD
#include <numeric>                        // Standard library import for std::accumulate
#include "pybind11/pybind11.h"            // Pybind11 import to define Python bindings
#include <xtensor/xbuilder.hpp>
#include <xtensor/xmath.hpp>              // xtensor import for the C++ universal functions
#include <xtensor/xslice.hpp>
#include <xtensor/xview.hpp>
#define FORCE_IMPORT_ARRAY                // numpy C api loading
#include <xtensor-python/pyarray.hpp>   
#include <xtensor-python/pytensor.hpp>   
#include <iostream>
#include<omp.h>
#include <list>

//  std::cerr<<omp_get_num_devices()<<"\n";
//  std::cerr<<omp_get_num_threads()<<"\n"<<omp_get_thread_limit()<<"\n"<<omp_get_num_procs()<<"\n";

using namespace xt::placeholders;

// based on code from https://github.com/stardist/stardist/blob/master/stardist/lib/stardist2d.cpp
xt::pytensor<int, 4> stardist(xt::pytensor<float, 2> img, size_t n_rays = 16, double background_level=0.001, double border_level=0.3, size_t max_steps = 100)
{
  int16_t bodyCost = -1, borderCost = 30, backgroundCost = 2, borderAward=-40;
  xt::xtensor<int, 4> dst ({img.shape(0), img.shape(1), n_rays, max_steps});
  int32_t shape0 = img.shape(0);
  int32_t shape1 = img.shape(1);
  #pragma omp distribute parallel for schedule(dynamic)
  for (int32_t i=0; i<shape0; i++)
  {
    for (int32_t j=0; j<shape1; j++)
    {
      const float value = img(i,j);
      if (value < background_level)
      {
        xt::view(dst, i,j, xt::all(), xt::all()) = INT_MAX;
      }
      // foreground pixel
      else
      {
        const float st_rays = (2*M_PI) / n_rays; // step size for ray angles
        for (uint32_t k = 0; k < n_rays; k++) {
          const float phi = k*st_rays;
          const float dy = cos(phi);
          const float dx = sin(phi);
          bool lastWasBorder = false;
          int lastCost = 0;
          float x = 0, y = 0;
          dst(i,j,k,0)=lastCost;
          // move along ray
          for (uint32_t p=1; p<max_steps; p++) {
            x += dx;
            y += dy;
            const int ii = round(i+x), jj = round(j+y);
            // stop if out of bounds
            if (ii < 0 || ii >= shape0 || jj < 0 || jj >= shape1)
            {
              break;
            }
            const float curr_val = img(ii,jj);
            int16_t dCost = lastWasBorder ? borderCost-borderAward : 0;
            lastWasBorder=false;
            if (curr_val<background_level)
              dCost += backgroundCost;
            else if(curr_val > border_level)
            {
              dCost += borderAward;
              lastWasBorder = true;
            }
            else
              dCost += bodyCost;
            lastCost +=dCost;
            dst(i,j,k,p) = lastCost;
          }
        }
      }
    }
  }
  return dst;
}

auto find_argmin_around(xt::xtensor<int, 2> dst, int max_diff, int start_approx, int shape1, int i)
{
  int start=start_approx;
  for(int k=-max_diff; k<=max_diff;k++)
  {
    int ns = start_approx+k;
    if (ns<0)
      continue;
    if(ns>=shape1)
      break;
    if (dst(i,ns) < dst(i, start))
      start = ns;
  }
  return start;
}

auto dynamic_reverse_pass(xt::xtensor<int, 2> dst, int max_diff, int start_approx)
{
  int32_t shape0 = dst.shape(0);
  int32_t shape1 = dst.shape(1);

  int start = start_approx;
  xt::xtensor<int, 1> path(xt::shape({shape0-1}));
  for (int i = shape0-2;i>=0;i--)
  {
    start = find_argmin_around(dst, max_diff, start, shape1, i);
    path(i)=start;
  }
  return path;
}

xt::xtensor<int, 2>  dynamic_analyze_starting_point(xt::xtensor<int, 2> costs, int max_diff=2)
{
  int32_t shape0 = costs.shape(0); //n_rays
  int32_t shape1 = costs.shape(1); //max_steps


  xt::xtensor<int, 2> results (xt::shape({shape1, shape0+1}));
  for (int d=0; d<shape1; d++)
  {
    xt::xtensor<int,2> dst = xt::empty<int>({shape0+1, shape1});
    xt::view(dst, xt::all(), xt::all()) = INT_MAX;
    dst(0, d)=0;
    for(int j=0; j<shape1; j++)
    {
      for (int i=0; i< shape0; i++)
      {
        int new_cost = INT_MAX;
        for(int k=-max_diff; k<=max_diff;k++)
        {
          int nj = j+k;
          if (nj<0)
            continue;
          if(nj>=shape1)
            break;
          if (dst(i,nj)==INT_MAX or costs(i,nj)==INT_MAX)
            continue;
          new_cost = std::min(new_cost, dst(i, nj) + costs(i,nj));
        }
        dst(i+1,j) = new_cost;
      }
    }

    results(d,0) = dst(shape0, find_argmin_around(dst, max_diff, d, shape1, shape0));
    xt::view(results, d, xt::range(1, xt::placeholders::_)) = dynamic_reverse_pass(dst, max_diff, d);
  }
//  std::cerr<<"Kon\n";
  return results;
}

xt::pytensor<int, 4> analyse_dst(xt::pytensor<int, 4> dst, int max_diff= 2)
{
  size_t shape0 = dst.shape(0);
  size_t shape1 = dst.shape(1);
  xt::xtensor<int, 4> results (xt::shape({shape0, shape1, dst.shape(3), dst.shape(2)+1}));
  
  std::cerr<<"TUTAJ3\n";
  
  #pragma omp distribute parallel for schedule(dynamic)
  for (size_t i=0;i<shape0;i++)
  {
    for(size_t j = 0; j<shape1; j++)
    {
      xt::view(results, i, j, xt::all(), xt::all()) = dynamic_analyze_starting_point(xt::view(dst, i, j, xt::all(), xt::all()), max_diff);
    }
  }
//  std::cerr<<"TUTAJ\n";
//  std::cerr<<results.shape(0)<<results.shape(1)<<results.shape(2)<<"\n";
  return results;
}

double sum_of_sines(xt::pyarray<double>& m)
{
    auto sines = xt::sin(m);  // sines does not actually hold values.
    std::cerr<<"BOO\n";
    return std::accumulate(sines.cbegin(), sines.cend(), 0.0);
}

PYBIND11_MODULE(my_starconvex, m)
{
    xt::import_numpy();
    m.doc() = "Test module for xtensor python bindings";

    m.def("sum_of_sines", sum_of_sines, "Sum the sines of the input values");
    m.def("stardist", stardist, "Star distance for all points.");
    m.def("dynamic_analyze_starting_point", dynamic_analyze_starting_point);
    m.def("analyse_dst", analyse_dst);
}
