#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <cmath>
#include <iostream>
#include <fstream>

namespace py=boost::python;
namespace np = boost::python::numpy;

typedef int8_t typWag;

class wagiInterwalow 
{
//  static int maly;
//  static int wielki; 
  public:
  static int czysty;
};

int wagiInterwalow::czysty=4;
double karaZaCisze=-10;

std::vector<std::vector<std::pair<int, typWag>>> macierzCpp;

void stworzMacierz(py::list macierzKonsonanow,int wagaCzystego, double karaCiszy)
{
  karaZaCisze=karaCiszy;
  wagiInterwalow::czysty=wagaCzystego;
  int dlugosc= py::len(macierzKonsonanow);
  macierzCpp.resize(dlugosc);
  for (int i=0;i<dlugosc;i++)
  {
    py::list podlista = py::extract<py::list>(macierzKonsonanow[i]);
    int dlugoscPodlisty = py::len(podlista);
    macierzCpp[i].resize(dlugoscPodlisty);
    for (int j=0;j<dlugoscPodlisty;j++)
    {
      std::pair<int, typWag> krotka {py::extract<int>(podlista[j][0]),py::extract<typWag>(podlista[j][1])} ;
      macierzCpp[i][j]=krotka;
    }
  }
}

int testMacierzy()
{
  int suma=0;
  for (auto& w : macierzCpp)
  {
    for (auto& para : w)
    {
      suma+=para.first;
    }
  }
  return suma;
}


/// Funkcja oceny przyjmuje spektogram w postaci ndarray i
/// ocenia go na podstawie liczby konsonansów
double funkcjaOceny(np::ndarray spektogram)
{
  int kolumny = spektogram.shape(1);

  int liczbaRozwazanychWierszy=macierzCpp.size();
  
  double wynik=0;
  for(int i=0;i<liczbaRozwazanychWierszy;i++)
  {
    for(int j=0;j<kolumny-1;j++)
    {
      float aktualnePole=py::extract<float>(spektogram[i][j]);
      wynik+=(typWag)wagiInterwalow::czysty*std::min(aktualnePole,(float)py::extract<float>(spektogram[i][j+1]));
      for(const auto& para: macierzCpp[i])
      {
        float minZF=std::min(aktualnePole,(float)py::extract<float>(spektogram[para.first][j]));
        wynik+=minZF*para.second;
        minZF=std::min(aktualnePole,(float)py::extract<float>(spektogram[para.first][j+1]));
        wynik+=minZF*para.second;
      }
    }
  }
  return wynik;
}
int flagi(np::ndarray macierz)
{
  return (int)macierz.get_flags();
}
double funkcjaOcenyPtr(np::ndarray spektogram)
{
  if(spektogram.get_flags() !=14 )
  {
    throw std::runtime_error("Bledne flagi");
  }
  int wiersze = spektogram.shape(0);
  int kolumny = spektogram.shape(1);

  int liczbaRozwazanychWierszy=macierzCpp.size();
  
  double wynik=0;
  double suma=0;
  float* tablica = (float*)spektogram.get_data();
  for(int j=0;j<kolumny-1;j++)
  {
    for(int i=0;i<liczbaRozwazanychWierszy;i++)
    {
      float aktualnePole= *(tablica+i+j*wiersze);
      suma+=std::fabs(aktualnePole);
      wynik+=(typWag)wagiInterwalow::czysty*std::min(aktualnePole,*(tablica+i+(j+1)*wiersze));
      for(const auto& para: macierzCpp[i])
      {
        float minZF=std::min(aktualnePole,*(tablica+para.first+j*wiersze));
        wynik+=minZF*para.second;
        minZF=std::min(aktualnePole,*(tablica+para.first+(j+1)*wiersze));
        wynik+=minZF*para.second;
      }
    }
    if (suma<1.0 and j>10 and j<kolumny-10)
    {
      wynik+=liczbaRozwazanychWierszy*karaZaCisze;
    }
  }
  return wynik;
}
BOOST_PYTHON_MODULE(funkcjeCpp)
{
  np::initialize();
  using namespace boost::python;
  def("funkcjaOceny", funkcjaOceny);
  def("funkcjaOcenyPtr", funkcjaOcenyPtr);
  def("testMacierzy", testMacierzy);
  def("stworzMacierz", stworzMacierz);
  def("flagi", flagi);
}

